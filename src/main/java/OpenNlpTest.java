import opennlp.tools.cmdline.PerformanceMonitor;
import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;
import opennlp.tools.util.Span;

import java.io.*;

public class OpenNlpTest {
    public static void main(String[] args) throws IOException {

//        tokenizer();
        tokenizerModel();
        sentenceDetector();
        partOfSpeech();
        nameFinder();
    }

    private static void partOfSpeech() throws IOException{
        POSModel model = new POSModelLoader().load(new File("src/main/resources/en-pos-maxent.bin"));
        PerformanceMonitor perfMon = new PerformanceMonitor(System.err, "sent");
        POSTaggerME tagger = new POSTaggerME(model);

        String input = "Can anyone help me dig through OpenNLP's horrible documentation?";
        ObjectStream<String> lineStream =
                new PlainTextByLineStream(new StringReader(input));

        perfMon.start();
        String line;
        while ((line = lineStream.read()) != null) {

            String[] whitespaceTokenizerLine = WhitespaceTokenizer.INSTANCE.tokenize(line);
            String[] tags = tagger.tag(whitespaceTokenizerLine);

            POSSample sample = new POSSample(whitespaceTokenizerLine, tags);
            System.out.println(sample.toString());

            perfMon.incrementCounter();
        }
        perfMon.stopAndPrintFinalResult();
    }
    private static void sentenceDetector() throws IOException {

        String paragraph = "Hi. How are you? This is Ashish.";
        InputStream is = new FileInputStream("src/main/resources/en-sent.bin");
        SentenceModel model = new SentenceModel(is);
        SentenceDetectorME sdetector = new SentenceDetectorME(model);

        String[] sentences = sdetector.sentDetect(paragraph);

        System.out.println(sentences[0]);
        System.out.println(sentences[1]);
        is.close();
    }

    private static void tokenizer() {
        String sentence = "A sentence has a subject (the person, place or thing that the sentence is about)" +
                "and an action (what the subject is doing)";

        //Instantiating whitespaceTokenizer class
        WhitespaceTokenizer whitespaceTokenizer = WhitespaceTokenizer.INSTANCE;

        //Tokenizing the given paragraph
        String[] tokens = whitespaceTokenizer.tokenize(sentence);

        //Printing the tokens
        for(String token : tokens)
            System.out.println(token);
    }

    private static void tokenizerModel() throws IOException {
        String sent = "A sentence has a subject (the person, place or thing that the sentence is about)" +
                " and an action (what the subject is doing)";

        //Loading the Tokenizer model
        InputStream inputStream = new FileInputStream("src/main/resources/en-token.bin");
        TokenizerModel tokenModel = new TokenizerModel(inputStream);

        //Instantiating the TokenizerME class
        TokenizerME tokenizer = new TokenizerME(tokenModel);

        //Retrieving the positions of the tokens
        Span[] tokens = tokenizer.tokenizePos(sent);

        //Printing the spans of tokens
        for(Span token : tokens)
            System.out.println(token +" "+sent.substring(token.getStart(), token.getEnd()));
    }

    private static void nameFinder() throws IOException{
        //Loading the NER - Person model
        InputStream inputStream = new
                FileInputStream("src/main/resources/en-ner-person.bin");
        TokenNameFinderModel model = new TokenNameFinderModel(inputStream);

        //Instantiating the NameFinder class
        NameFinderME nameFinder = new NameFinderME(model);

        //Getting the sentence in the form of String array
        String [] sentence = new String[]{
                "Mike",
                "and",
                "Smith",
                "are",
                "good",
                "friends"
        };

        //Finding the names in the sentence
        Span[] nameSpans = nameFinder.find(sentence);

        //Printing the spans of the names in the sentence
        for(Span s: nameSpans)
            System.out.println(s.toString());
    }
}

/*  ------------------------------------------------

            PART OF Speech (POS)

    |    NN 	|    Noun, singular or mass
    |    DT 	|    Determiner
    |    VB 	|    Verb, base form
    |    VBD 	|    Verb, past tense
    |    VBZ 	|    Verb, third person singular present
    |    IN 	|    Preposition or subordinating conjunction
    |    NNP 	|    Proper noun, singular
    |    TO 	|    to
    |    JJ 	|    Adjective

---------------------------------------------------- */
